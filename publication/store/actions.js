/* NOTE: this uses both default axios and a custom instance
 * of axios defined in /frontend/plugins/axios.js
 */
import axios from 'axios'
import api from '@/plugins/axios'

// NOTE: various utils for handling hand off to / from api
import {isObjectEmpty} from '@/utils/index.js'

const actions = {

}

export default actions
